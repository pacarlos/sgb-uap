package pe.com.uap.sgb.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import pe.com.uap.sgb.dao.PersonDao;
import pe.com.uap.sgb.enums.TypePerson;
import pe.com.uap.sgb.model.Person;
import pe.com.uap.sgb.model.Student;
import pe.com.uap.sgb.model.Teacher;

public interface PersonService
	{
		
		public List<Person> findAll();
		
		public void action(String dni, String name, String lastName,
				String email, String phone, String numberEmployees,
				String registration, TypePerson person);

		public Person find(Person person);
	}

@Stateless
class PersonServiceImpl implements PersonService
	{
		@Inject
		private PersonDao	dao;
		
		public List<Person> findAll()
			{
				return dao.findAll();
			}
		
		@Override
		public void action(String dni, String name, String lastName,
				String email, String phone, String numberEmployees,
				String registration, TypePerson typePerson)
			{
				boolean key = false;
				if (dni != null)
					{
						Person person = dao.find(new Person(Integer
								.parseInt(dni)));
						
						if (person != null)
							{
								key = true;
								
							} else
							{
								person = new Person();
								person.setDni(Integer.parseInt(dni));
								key = false;
							}
						
					person.setEmail(email);
					person.setLastName(lastName);
					person.setName(name);
					person.setTypePerson(typePerson);
					person.setPhone(Integer.parseInt(phone));
					person.setNumberBox(0);
					if(typePerson == TypePerson.STUDEN){
						Student student = new Student();
						student.setDni(Integer.parseInt(dni));
						student.setPerson(person);
						student.setRegistration(Integer.parseInt(registration));
						person.setStudent(student);
					}else{
						Teacher teacher = new Teacher();
						teacher.setDni(Integer.parseInt(dni));
						teacher.setNumberEmployees(Integer.parseInt(numberEmployees));
						teacher.setPerson(person);
					}
					if(!key){
						dao.save(person);
					}else{
						dao.update(person);
					}
				}
			}

		@Override
		public Person find(Person person)
			{
				return dao.find(person);
			}
	}
