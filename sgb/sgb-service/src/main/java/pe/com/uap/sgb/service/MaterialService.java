package pe.com.uap.sgb.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import pe.com.uap.sgb.dao.MaterialDao;
import pe.com.uap.sgb.enums.TypeMaterial;
import pe.com.uap.sgb.enums.TypeStatus;
import pe.com.uap.sgb.model.Box;
import pe.com.uap.sgb.model.Magazine;
import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.service.interfaces.Service;

public interface MaterialService extends Service
	{
		
		public Long count();
		
		public Material find(Material material);
		
		public List<Material> findAll();
		
		public void action(String idMaterial, String author, String title,
				String ao, TypeMaterial type, TypeStatus status,
				String editorial);

		public List<Material> find(String author, String title, String editorial);
		
	}

@Stateless
class MaterialServiceImpl implements MaterialService
	{
		@Inject
		private MaterialDao	dao;
		
		public void save(Material material)
			{
				dao.save(material);
			}
		
		@Override
		public Material find(Material material)
			{
				return dao.find(material);
			}
		
		@Override
		public List<Material> findAll()
			{
				return dao.findAll();
			}
		
		@Override
		public Long count()
			{
				
				return dao.count();
			}
		
		public String id()
			{
				return dao.id();
			}
		
		@Override
		public void action(String idMaterial, String author, String title,
				String ao, TypeMaterial type, TypeStatus status,
				String editorial)
			{
				
				Material material = new Material();
				
				if (idMaterial == null)
					{
						material.setIdMaterial(id());
					} else
					{
						material.setIdMaterial(idMaterial);
					}
				
				material.setAutor(author);
				material.setTitle(title);
				material.setAge(Integer.parseInt(ao));
				material.setTypeMaterial(type);
				material.setStatus(status);
				
				validateTypeMaterial(material, editorial);
				
				if (idMaterial != null)
					{
						
						update(material);
					} else
					{
						save(material);
					}
				
			}
		
	
		
		private void validateTypeMaterial(Material material, String editorial)
			{
				if (material.getTypeMaterial() == TypeMaterial.BOX)
					{
						
						Box box = new Box();
						box.setEditorial(editorial);
						material.setBox(box);
					} else
					{
						Magazine magazine = new Magazine();
						material.setMagazine(magazine);
					}
			}
		
		public void update(Material material)
			{
				dao.update(material);
				
			}
		
		@Override
		public List<Material> find(String author, String title, String editorial)
			{
				Material material = new Material();
				if (author != null)
					material.setAutor(author);
				if (title != null)
					material.setTitle(title);
				Box box = new Box();
				if (editorial != null)
					box.setEditorial(editorial);
				material.setBox(box);
				
				return dao.findMaterial(material);
				
				
			}
		
	}
