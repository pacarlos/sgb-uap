package pe.com.uap.sgb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import pe.com.uap.sgb.dao.LoanDao;
import pe.com.uap.sgb.model.Loan;
import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.model.Person;

public interface LoanService
	{
		
		public boolean carry(Person person, Material material,Date departureDate);
		
		public boolean deliver(Loan loan);
		
		public List<Loan> findAllByPerson(Person person);
		
		public List<Loan> findAll();
		
	}

@Stateless
class LoanServiceImpl implements LoanService
	{
		@Inject
		private LoanDao	dao;
		
		@Override
		public boolean carry(Person person, Material material,
				Date departureDate)
			{
				try
					{
						person.carry();
						
						Loan loan = new Loan();
						loan.setPerson(person);
						loan.setMaterial(material);
						loan.setDepartureDate(departureDate);
						loan.setReturnDate(null);
						if (dao.save(loan) != null)
							{
								return true;
							}
						return false;
					} catch (Exception e)
					{
						return false;
					}
				
			}
		
		public String generateTicket()
			{
				return dao.id();
			}
		
		@Override
		public boolean deliver(Loan loan)
			{
				try
					{
						loan.setReturnDate(new Date());
						loan.getPerson().deliver();
						if (dao.update(loan) != null)
							{
								return true;
							}
						return false;
					} catch (Exception e)
					{
						return false;
					}
				
			}
		
		@Override
		public List<Loan> findAll()
			{
				return dao.findAll();
			}
		
		@Override
		public List<Loan> findAllByPerson(Person person)
			{
			
				if (person != null)
					{
						return dao.findAllByPerson(person);	
					}
			return new ArrayList<Loan>();	
			}
		
	}
