package pe.com.uap.form;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import pe.com.uap.form.element.SearchModel;
import pe.com.uap.form.event.Model;
import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.service.LoanService;
import pe.com.uap.sgb.service.MaterialService;

public class SearchForm extends Container
	{
		
		/**
	 * 
	 */
		private static final long	serialVersionUID	= 1L;
		private JTable				table				= new JTable();
		private GroupLayout			groupLayout			= new GroupLayout(this);
		private Model<SearchModel>	model				= new Model<SearchModel>();
		private JLabel				lblBusqueda			= new JLabel("Busqueda");
		private JLabel				lblNewLabel			= new JLabel("...");
		private JButton				btnPrestar			= new JButton("Prestar");
		private MaterialService		materialService;
		private LoanService			loanService;
		
		/**
		 * Create the panel.
		 */
		public SearchForm(MaterialService materialService,
				LoanService loanService,String author,String title,String editorial)
			{
				this.materialService= materialService;
				this.loanService = loanService;
				model.column(SearchModel.class);
				model.addColumn("idMaterial", "codigo");
				model.addColumn("author", "autor");
				model.addColumn("title", "Titulo");
				model.addColumn("typeMaterial", "Tipo de material");
				model.addColumn("operaciones", "panel");
				
				btnPrestar.addActionListener(actionCarry());
				buildSearch(author, title, editorial);
				
				table.setModel(model);
				table.setRowSelectionAllowed(true);
				
				ListSelectionModel listSelectionModel = table.getSelectionModel();
				listSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				listSelectionModel.addListSelectionListener(actionListenerSelection());
				
				init();
			
			}
		
		public ActionListener actionCarry()
			{
				return new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
							{
								System.out.println(e);
							}
					};
			}
		
		private List<Material> findMaterial(String author, String title, String editorial)
			{
				return this.materialService.find(author,title , editorial);
			}
		
		private List<SearchModel> buildSearch(String author, String title, String editorial)
			{
				List<SearchModel> list = new ArrayList<SearchModel>();
				for (Material material : findMaterial(author, title, editorial))
					{
						SearchModel searchModel = new SearchModel(material);
						searchModel.setAuthor(material.getAutor());
						searchModel.setTitle(material.getTitle());
						searchModel.getPanel().add(btnPrestar);
						list.add(searchModel);
					}
				return list;
			}
		
		private ListSelectionListener actionListenerSelection()
			{
				return new ListSelectionListener()
					{
						
						@Override
						public void valueChanged(ListSelectionEvent e)
							{
								
							}
						
					};
			}
		
		public void init()
			{
				groupLayout
						.setHorizontalGroup(groupLayout
								.createParallelGroup(Alignment.LEADING)
								.addGroup(
										groupLayout
												.createSequentialGroup()
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.LEADING)
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(12)
																				.addComponent(
																						lblBusqueda,
																						GroupLayout.PREFERRED_SIZE,
																						56,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(18)
																				.addComponent(
																						lblNewLabel,
																						GroupLayout.DEFAULT_SIZE,
																						334,
																						Short.MAX_VALUE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(26)
																				.addComponent(
																						table,
																						GroupLayout.DEFAULT_SIZE,
																						394,
																						Short.MAX_VALUE)))
												.addGap(30))
								.addGroup(
										groupLayout
												.createSequentialGroup()
												.addGap(188)
												.addComponent(
														btnPrestar,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addGap(189)));
				groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
						Alignment.LEADING).addGroup(
						groupLayout
								.createSequentialGroup()
								.addGap(13)
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.LEADING)
												.addComponent(lblBusqueda)
												.addComponent(lblNewLabel))
								.addGap(27)
								.addComponent(table, GroupLayout.DEFAULT_SIZE,
										186, Short.MAX_VALUE).addGap(18)
								.addComponent(btnPrestar).addGap(15)));
				setLayout(groupLayout);
				
			}
	}
