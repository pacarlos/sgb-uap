package pe.com.uap.form.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import pe.com.uap.form.LoanForm;
import pe.com.uap.form.MaterialForm;
import pe.com.uap.form.PersonForm;
import pe.com.uap.form.Principal;
import pe.com.uap.form.SearchForm;
import pe.com.uap.form.element.TypeForm;
import pe.com.uap.sgb.service.LoanService;
import pe.com.uap.sgb.service.MaterialService;
import pe.com.uap.sgb.service.PersonService;

@ApplicationScoped
public class ListenerPrincipal
	{
		
		private MaterialForm	materialForm;
		
		private PersonForm		personForm;
		
		private LoanForm		loanForm;
		
		private SearchForm		searchForm;
		
		@Inject
		private MaterialService	materialService;
		
		@Inject
		private PersonService	personService;
		
		@Inject
		private LoanService		loanService;
		
		public ActionListener close()
			{
				
				return new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
							{
								System.exit(0);
								
							}
					};
			}
		
		public ActionListener displayForm(final TypeForm form,
				final Principal principal)
			{
				
				return new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
							{
								switch (form)
									{
										case LOAN:
											loanForm = new LoanForm(loanService);
											principal.addTab("Prestamos",loanForm);
											break;
										
										case MATERIAL:
											materialForm = new MaterialForm(materialService);
											materialForm.findAll();
											principal.addTab("Material",materialForm);
											break;
										
										case PERSON:
											personForm = new PersonForm(personService);
											personForm.findAll();
											principal.addTab("Personas",personForm);
											break;
										case SEARCH:
											searchForm = new SearchForm(materialService, loanService, "", "", "");
											principal.addTab("Buscar", searchForm);
											break;
									
									}
								
							}
					};
			}
		
	}
