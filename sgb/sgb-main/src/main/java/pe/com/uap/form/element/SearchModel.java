package pe.com.uap.form.element;

import java.io.Serializable;

import javax.swing.JPanel;

import pe.com.uap.sgb.model.Material;

public class SearchModel implements Serializable
	{
		/**
	 * 
	 */
		private static final long	serialVersionUID	= 1L;
		private String				idMaterial;
		private String				author;
		private String				title;
		private String				typeMaterial;
		private Material			material			= new Material();
		private JPanel				panel				= new JPanel();
		
		public SearchModel(Material material)
			{
				this.setIdMaterial(material.getIdMaterial());
				this.setAuthor(material.getAutor());
				this.setTitle(material.getTitle());
				this.setTypeMaterial(material.getTypeMaterial().name());
				
				this.material = material;
			}
		
		public String getIdMaterial()
			{
				return idMaterial;
			}
		
		public void setIdMaterial(String idMaterial)
			{
				this.idMaterial = idMaterial;
			}
		
		public String getAuthor()
			{
				return author;
			}
		
		public void setAuthor(String author)
			{
				this.author = author;
			}
		
		public String getTitle()
			{
				return title;
			}
		
		public void setTitle(String title)
			{
				this.title = title;
			}
		
		public String getTypeMaterial()
			{
				return typeMaterial;
			}
		
		public void setTypeMaterial(String typeMaterial)
			{
				this.typeMaterial = typeMaterial;
			}
		
		public JPanel getPanel()
			{
				return panel;
			}
		
		public void setPanel(JPanel panel)
			{
				this.panel = panel;
			}
		
	}
