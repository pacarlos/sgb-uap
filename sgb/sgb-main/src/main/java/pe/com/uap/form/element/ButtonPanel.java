package pe.com.uap.form.element;

import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class ButtonPanel extends Container
	{
		public enum Type
			{
				SAVE, NEW
			}
		
		private static final long	serialVersionUID	= 1L;
		private JButton				btnSave				= new JButton("Guardar");
		private JButton				btnNew				= new JButton("Nuevo");
		private GroupLayout			groupLayout			= new GroupLayout(this);
		
		/**
		 * Create the panel.
		 */
		public ButtonPanel()
			{
				
				init();
			}
		
		public void addActionListener(Type type, ActionListener actionListener)
			{
				switch (type)
					{
						case SAVE:
							
							btnSave.addActionListener(actionListener);
							break;
						case NEW:
							btnNew.addActionListener(actionListener);
							break;
					}
				
			}
		
		public void init()
			{
				groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(
						Alignment.LEADING).addGroup(
						groupLayout
								.createSequentialGroup()
								.addGap(27)
								.addComponent(btnSave,
										GroupLayout.DEFAULT_SIZE, 192,
										Short.MAX_VALUE)
								.addGap(12)
								.addComponent(btnNew, GroupLayout.DEFAULT_SIZE,
										192, Short.MAX_VALUE).addGap(27)));
				
				groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
						Alignment.LEADING).addGroup(
						groupLayout
								.createSequentialGroup()
								.addGap(29)
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.LEADING)
												.addComponent(btnSave)
												.addComponent(btnNew))));
				
				setLayout(groupLayout);
			}
		
		public JButton getBtnNew()
			{
				return btnNew;
			}
		
		public JButton getBtnSave()
			{
				return btnSave;
			}
		
	}
