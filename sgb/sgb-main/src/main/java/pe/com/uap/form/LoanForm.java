package pe.com.uap.form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.persistence.platform.database.SybasePlatform;

import pe.com.uap.form.element.ButtonPanel.Type;
import pe.com.uap.form.element.ContainerForm;
import pe.com.uap.form.element.LoanModel;
import pe.com.uap.sgb.model.Loan;
import pe.com.uap.sgb.model.Person;
import pe.com.uap.sgb.service.LoanService;
import pe.com.uap.sgb.service.PersonService;

public class LoanForm extends ContainerForm<LoanModel>
	{
		
		/**
	 * 
	 */
		private static final long	serialVersionUID	= 1L;
		
		private LoanService			loanService;
		
		private JTextField			dni					= new JTextField();
		private JLabel				ticket				= new JLabel();
		private JLabel				title				= new JLabel();
		
		private Loan				loan;
		
		// private final JTable jTable = new JTable();
		
		public LoanForm(LoanService loanService)
			{
				this.loanService = loanService;
								
				addInput("DNI : ", null, dni);
				addInput("Tiket : ", null, ticket);
				addInput("Titulo : ", null, title);
				
				addColumn("idLoan", "Ticket");
				addColumn("title", "Titulo");
				addColumn("numberBox", "Numero de Libros Prestados");
				addColumn("depeartureDate", "Fecha de Salida");
				addColumn("returnDate", "Fecha de entrega");
				
				addTable(LoanModel.class);
				addPanelButton();
				getButtonPanel().addActionListener(Type.NEW, actionNew());
				getBtnSave().setText("Devolver");
				getBtnNew().setText("Buscar por DNI");

				build();
			}
		
		@Override
		public ActionListener actionNew()
			{
				return new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
							{
								insertList();
								getTable().updateUI();
								getBtnNew().setEnabled(true);
							}
					};
				
			}
		
		@Override
		public ActionListener actionSave()
			{
				return new ActionListener()
					{
						
						public void actionPerformed(ActionEvent e)
							{
								buildObject();
								insertList();
								getBtnSave().setEnabled(false);
								clearObject();
								
							}
						
					};
			}
		
		@Override
		public void clearObject()
			{
				ticket.setText("");
				title.setText("");
				
			}
		
		@Override
		public List<LoanModel> findAll()
			{
				List<LoanModel> list = new ArrayList<>();
				if (dni.getText() != null && dni.getText().length() > 0)
					{
						Person person = new Person(Integer.parseInt(dni
								.getText()));
						
						for (Loan loan : loanService.findAllByPerson(person))
							{
								LoanModel loanModel = new LoanModel(loan);
								list.add(loanModel);
							}
					}

				return list;
			}
		
		@Override
		public void buildObject()
			{
				loanService.deliver(loan);
				getBtnSave().setEnabled(false);
				
			}
		
		@Override
		public ListSelectionListener actionListenerSelection()
			{
				
				return new ListSelectionListener()
					{
						
						@Override
						public void valueChanged(ListSelectionEvent e)
							{
								LoanModel loanModel = getRowTable();
								ticket.setText(loanModel.getIdLoan());
								title.setText(loanModel.getTitle());
								
								if (loanModel.getLoan().getReturnDate() == null
										|| loanModel.getLoan().getPerson()
												.getNumberBox() > 0)
									{
										getBtnSave().setEnabled(true);
										loan = loanModel.getLoan();
									}
								
							}
						
					};
			}
		
	}
