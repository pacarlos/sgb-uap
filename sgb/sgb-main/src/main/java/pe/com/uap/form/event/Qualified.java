package pe.com.uap.form.event;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;;
@Documented
@Retention(RUNTIME)
@Qualifier
public @interface Qualified
	{
		String name();
	}
