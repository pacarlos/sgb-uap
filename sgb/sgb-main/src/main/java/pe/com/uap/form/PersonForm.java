package pe.com.uap.form;

import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import pe.com.uap.form.element.ContainerForm;
import pe.com.uap.sgb.enums.TypePerson;
import pe.com.uap.sgb.model.Person;
import pe.com.uap.sgb.service.PersonService;

public class PersonForm extends ContainerForm<Person>
	{
		
		/**
	 * 
	 */
		private static final long		serialVersionUID	= 1L;
		
		private JTextField				dni					= new JTextField();
		private JTextField				name				= new JTextField();
		private JTextField				lastName			= new JTextField();
		private JTextField				email				= new JTextField();
		private JTextField				phone				= new JTextField();
		private JTextField				numberEmployees		= new JTextField();
		private JTextField				registration		= new JTextField();
		private JComboBox<TypePerson>	typePerson			= new JComboBox<TypePerson>();
		private PersonService			personService;
		
		public PersonForm(PersonService personService)
			{
				this.personService = personService;
				
				addInput("DNI :", "dni", dni);
				addInput("Nombre : ", "name", name);
				addInput("Apellidos : ", "lastName", lastName);
				addInput("Correo : ", "email", email);
				addInput("Telefono : ", "phone", phone);
				addInput("Tipo de Persona : ", "typePerson", typePerson);
				
				addInput("Matricula : ", null, registration);
				addInput("Numero de Empleado : ", null, numberEmployees);
				
				addTable(Person.class);
				
				addPanelButton();
				buildTypePerson();
				build();
				
			}
		
		private void buildTypePerson()
			{
				for (TypePerson type : TypePerson.values())
					{
						typePerson.addItem(type);
					}
			}
		
		@Override
		public void clearObject()
			{
				dni.setText("");
				name.setText("");
				lastName.setText("");
				email.setText("");
				phone.setText("");
				numberEmployees.setText("");
				registration.setText("");
				typePerson.setSelectedIndex(0);
			}
	
		@Override
		public List<Person> findAll()
			{
				return personService.findAll();
			}
		
		@Override
		public void buildObject()
			{
				TypePerson person = TypePerson.class.cast(typePerson
						.getSelectedItem());
				
				personService.action(dni.getText(), name.getText(),
						lastName.getText(), email.getText(), phone.getText(),
						numberEmployees.getText(), registration.getText(),
						person);
			}
		
		@Override
		public ListSelectionListener actionListenerSelection()
			{
				
				return new ListSelectionListener()
					{
						
						@Override
						public void valueChanged(ListSelectionEvent e)
							{
								clearObject();
								Person person = getRowTable();
								dni.setText(person.getDni() + "");
								name.setText(person.getName());
								lastName.setText(person.getLastName());
								email.setText(person.getEmail());
								phone.setText(person.getPhone() + "");
								
								if (person.getTeacher() != null)
									{
										numberEmployees.setText(person
												.getTeacher()
												.getNumberEmployees()
												+ "");
									}
								
								if (person.getStudent() != null)
									{
										registration.setText(person
												.getStudent().getRegistration()
												+ "");
									}
								
								typePerson.setSelectedItem(person
										.getTypePerson());
								
								getBtnSave().setEnabled(true);
							}
						
					};
			}
		
	}
