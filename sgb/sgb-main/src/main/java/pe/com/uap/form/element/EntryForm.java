package pe.com.uap.form.element;



public class EntryForm<K, V>
	{
		private K	key;
		private V	value;
		
		public EntryForm(K key, V value)
			{
				this.key = key;
				this.value = value;
			}

		public K getKey()
			{
				return key;
			}

		public V getValue()
			{
				return value;
			}
		
	}
