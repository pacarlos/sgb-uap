package pe.com.uap.form.element;

import java.awt.Component;
import java.awt.Container;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import pe.com.uap.form.element.ButtonPanel.Type;
import pe.com.uap.form.event.Model;

public abstract class ContainerForm<T> extends Container
	{
		
		private static final long							serialVersionUID	= 1L;
		
		private Map<String, EntryForm<JLabel, Component>>	mapComponent		= new LinkedHashMap<>();
		private ButtonPanel									buttonPanel			= new ButtonPanel();
		private JTable										table				= new JTable();
		private Model<T>									model				= new Model<T>();
		
		private GroupLayout									groupLayout;
		
		public ContainerForm()
			{
				
				setBounds(100, 100, 450, 300);
				groupLayout = new GroupLayout(this);
				
			}
		public Model<T> getModel()
			{
				return model;
			}
		public JTable getTable()
			{
				return table;
			}
		public ParallelGroup parallelGroup()
			{
				return groupLayout.createParallelGroup();
				
			}
		
		public SequentialGroup sequentialGroup()
			{
				return groupLayout.createSequentialGroup();
			}
		
		public SequentialGroup createSequentialGroup(JLabel label,
				Component component)
			{
				
				return sequentialGroup()
						.addGap(36)
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 118,
								GroupLayout.PREFERRED_SIZE)
						.addGap(31)
						.addComponent(component, GroupLayout.DEFAULT_SIZE, 237,
								Short.MAX_VALUE);
			}
		
		public ParallelGroup createParallelGroup(JLabel label,
				Component component)
			{
				return parallelGroup().addComponent(label).addComponent(
						component, GroupLayout.PREFERRED_SIZE,
						GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);
			}
		
		public void addInput(String name, String reference, Component component)
			{
				if (!mapComponent.containsKey(name))
					{
						mapComponent.put(name,
								new EntryForm<JLabel, Component>(new JLabel(
										name), component));
						
					}
				addColumn(reference, name);
				
			}
		
		public void addColumn(String reference, String name)
			{
				if (reference != null)
					{
						model.addColumn(reference, name);
					}
			}
		
		public SequentialGroup sequentialGroup(ParallelGroup parallelGroup)
			{
				return sequentialGroup().addGroup(parallelGroup);
			}
		
		public ParallelGroup parallelGroup(SequentialGroup group)
			{
				return parallelGroup().addGroup(group);
			}
		
		public void addPanelButton()
			{
				buttonPanel.getBtnSave().setEnabled(false);
				
				buttonPanel.addActionListener(Type.NEW, actionNew());
				buttonPanel.addActionListener(Type.SAVE, actionSave());
				
				addInput("Operaciones", null, buttonPanel);
			}
		public ButtonPanel getButtonPanel()
			{
				return buttonPanel;
			}
		public void addTable(Class<T> cls)
			{
				
				model.column(cls);
				
				table.setModel(model);
				table.setRowSelectionAllowed(true);
				
				ListSelectionModel listSelectionModel = table
						.getSelectionModel();
				listSelectionModel
						.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				listSelectionModel
						.addListSelectionListener(actionListenerSelection());
				
				insertList();
				
				addInput("Lista : ", null, new JScrollPane(table));
				
			}
		
		public void insertList()
			{
				model.add(findAll());
				table.clearSelection();
				table.updateUI();
			}
		
		public int indexTable()
			{
				ListSelectionModel selectionModel = table.getSelectionModel();
				return selectionModel.getLeadSelectionIndex();
			}
		
		@SuppressWarnings("unchecked")
		public <T> T getRowTable()
			{
				return (T) model.get(indexTable());
			}
		
		public void build()
			{
				
				ParallelGroup parallelGroup = parallelGroup();
				
				SequentialGroup sequentialGroup = sequentialGroup();
				
				for (EntryForm<JLabel, Component> entryForm : mapComponent
						.values())
					{
						
						parallelGroup.addGroup(createSequentialGroup(
								entryForm.getKey(), entryForm.getValue()));
						sequentialGroup.addGap(24).addGroup(
								createParallelGroup(entryForm.getKey(),
										entryForm.getValue()));
						
					}
				
				groupLayout.setHorizontalGroup(parallelGroup(sequentialGroup(
						parallelGroup).addContainerGap()));
				groupLayout.setVerticalGroup(sequentialGroup);
				
				setLayout(groupLayout);
			}
		
		public ParallelGroup createParallelGroup(SequentialGroup sequentialGroup)
			{
				return parallelGroup().addGroup(
						sequentialGroup.addContainerGap());
			}
		
		public ActionListener actionNew()
			{
				return new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
							{
								clearObject();
								buttonPanel.getBtnNew().setEnabled(false);
								buttonPanel.getBtnSave().setEnabled(true);
							}
						
					};
			}
		
		public ActionListener actionSave()
			{
				return new ActionListener()
					{
						
						public void actionPerformed(ActionEvent e)
							{
								buildObject();
								insertList();
								getBtnSave().setEnabled(false);
								getBtnNew().setEnabled(true);
								clearObject();
							}
						
					};
				
			}
		
		public JButton getBtnSave()
			{
				return buttonPanel.getBtnSave();
				
			}
		
		public JButton getBtnNew()
			{
				return buttonPanel.getBtnNew();
			}
		
		public abstract void clearObject();
		
		public abstract List<T> findAll();
		
		public abstract void buildObject();
		
		public abstract ListSelectionListener actionListenerSelection();
	}
