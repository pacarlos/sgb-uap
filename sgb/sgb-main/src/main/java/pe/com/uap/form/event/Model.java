package pe.com.uap.form.event;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import pe.com.uap.form.element.EntryForm;

public class Model<T> extends AbstractTableModel
	{
		
		private static final long	serialVersionUID	= 1L;
		
		private List<T>				list;
		
		private Map<String, Field>	fields				= new LinkedHashMap<>();
		
		private Map<String, String>	columns				= new LinkedHashMap<>();
		
		public void add(List<T> list)
			{
				
				this.list = list;
				
				
			}
		
		public void addColumn(String key, String nameColumn)
			{
				columns.put(key, nameColumn);
				
			}
		
		public void column(Class<?> cls)
			{
				for (Entry<String, String> entry : columns.entrySet())
					{
						try
							{
								fields.put(entry.getKey(), cls.getDeclaredField(entry.getKey()));

							} catch (NoSuchFieldException | SecurityException e)
							{
								e.printStackTrace();
							}
					}
				
			}
		
		@Override
		public String getColumnName(int column)
			{
				return (String) columns.values().toArray()[column]; 
			}
		
		@Override
		public Class<?> getColumnClass(int columnIndex)
			{
				Field f = (Field) fields.values().toArray()[columnIndex];
				return f.getType();
			}
		
		@Override
		public int getColumnCount()
			{
				return fields.size();
			}
		
		@Override
		public int getRowCount()
			{
				if (list != null)
					{
						return list.size();
					}
				return 0;
			}
		
		public T get(int index)
			{
				return list.get(index);
			}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
			{
				if (list != null && list.size() > 0)
					{
						T row = list.get(rowIndex);
						try
							{
								Field field = (Field) fields.values().toArray()[columnIndex];
								field.setAccessible(true);
								return field.get(row);
								
							} catch (Exception e)
							{
								e.printStackTrace();
							}
					}
				
				return null;
			}
		
		public void insert(T row)
			{
				if (row != null)
					{
						if (!list.contains(row))
							{
								list.add(row);
							} else
							{
								int index = list.indexOf(row);
								list.add(index, row);
							}
						
					}
			}
	}