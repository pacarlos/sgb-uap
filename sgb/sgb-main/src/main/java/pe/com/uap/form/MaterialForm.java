package pe.com.uap.form;

import java.util.List;
import javax.inject.Inject;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import pe.com.uap.form.element.ContainerForm;
import pe.com.uap.sgb.enums.TypeMaterial;
import pe.com.uap.sgb.enums.TypeStatus;
import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.service.MaterialService;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MaterialForm extends ContainerForm<Material>
	{
		private JLabel					ID					= new JLabel();
		private JTextField				author				= new JTextField();
		private JTextField				title				= new JTextField();
		private JTextField				ao					= new JTextField();
		private JTextField				editorial			= new JTextField();
		private JComboBox<TypeStatus>	status				= new JComboBox<TypeStatus>();
		private JComboBox<TypeMaterial>	typeMaterial		= new JComboBox<TypeMaterial>();
		
		
		
	
		private MaterialService			materialService;
		
		private static final long		serialVersionUID	= 1L;
		
		@Inject
		public MaterialForm(MaterialService materialService)
			{
				this.materialService = materialService;
			
				addInput("ID :","idMaterial", ID);
				addInput("Autor : ", "autor", author);
				addInput("Titulo : ","title", title);
				addInput("A�o : ","age", ao);
				addInput("Editorial : ",null, editorial);
				addInput("Estado : ",null, status);
				addInput("Tipo de Material : ","typeMaterial", typeMaterial);
				
				addTable(Material.class);
				addPanelButton();
				

				build();
				
				buildTypeMaterial();
				buildTypeStatus();
			}
		

		
		public void clearObject()
			{
				ID.setText(null);
				author.setText("");
				title.setText("");
				ao.setText("");
				editorial.setText(null);
				typeMaterial.areFocusTraversalKeysSet(0);
				status.areFocusTraversalKeysSet(0);
			}
		
		public void buildObject()
			{
				TypeMaterial type = TypeMaterial.class.cast(typeMaterial.getSelectedItem());
				TypeStatus typeStatus = TypeStatus.class.cast(status.getSelectedItem());
				
				materialService.action(ID.getText(),author.getText(),title.getText(),ao.getText(),type,typeStatus,editorial.getText());	
			}

		public void buildTypeMaterial()
			{
				for (TypeMaterial type : TypeMaterial.values())
					{
						typeMaterial.addItem(type);
					}
			}
		
		public void buildTypeStatus()
			{
				for (TypeStatus type : TypeStatus.values())
					{
						status.addItem(type);
					}
			}

		public ListSelectionListener actionListenerSelection()
			{
				return new ListSelectionListener()
					{
						
						@Override
						public void valueChanged(ListSelectionEvent e)
							{
								if (!e.getValueIsAdjusting())
									{
										clearObject();
										Material material = getRowTable();
										ID.setText(material.getIdMaterial());
										author.setText(material.getAutor());
										title.setText(material.getTitle());
										ao.setText(material.getAge()+"");
										status.setSelectedItem(material.getStatus());
										
										typeMaterial.setSelectedItem(material.getTypeMaterial());
										
										if(material.getBox() != null){
											 editorial.setText(material.getBox().getEditorial());
										}
										
										getBtnSave().setEnabled(true);
									}
								
							}
					};
				
			}

		public List<Material>  findAll()
			{
				return materialService.findAll();
			
			}
	}
