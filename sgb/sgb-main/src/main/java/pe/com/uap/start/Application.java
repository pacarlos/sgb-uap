
package pe.com.uap.start;

import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.weld.environment.se.events.ContainerInitialized;

import pe.com.uap.form.Principal;
import static pe.com.uap.form.element.TypeForm.*;
import pe.com.uap.form.event.ListenerPrincipal;

@ApplicationScoped
public class Application
	{
		private static Logger		logger	= Logger.getLogger(Application.class
													.getName());
		
		@Inject
		private Principal			principal;
		
		@Inject
		private ListenerPrincipal	listener;
		
		public void start(@Observes ContainerInitialized startEvent)
			{
				
				startEventLoop(new Runnable()
					{
						public void run()
							{
								principal.addMenu("Inicio").addMenuItem("Salir", listener.close()).build();
								
								principal.addMenu("Mantenimiento");
								principal.addMenuItem("Material", listener.displayForm(MATERIAL, principal));
								principal.addMenuItem("Prestamos",listener.displayForm(LOAN, principal));
								principal.addMenuItem("Personas", listener.displayForm(PERSON, principal));
								principal.build();
								
								principal.display();
							}
					});
			}
		
		private void startEventLoop(final Runnable callback)
			{
				java.awt.EventQueue.invokeLater(callback);
				logger.log(Level.ALL, callback.toString());
			}
		
		public void updateValueWhenChangeValueAction(
				@Observes final EventListener action)
			{
				logger.log(Level.ALL, action.toString());
			}
	}
