package pe.com.uap.form;

import javax.swing.JFrame;

import java.awt.Button;
import java.awt.Component;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import pe.com.uap.form.event.ButtonTabComponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Principal extends JFrame
	{
		
		private static final long	serialVersionUID	= 1L;
		
		private JTextField			jTextAuthor			= new JTextField();
		private JTextField			jTextTitle			= new JTextField();
		private JTextField			jTextEditorial		= new JTextField();
		
		private JTabbedPane			tabContainerFrom	= new JTabbedPane(
																JTabbedPane.TOP);
		private JLabel				lblBuscar			= new JLabel(
																"Datos de busqueda");
		private JLabel				lblAuthor			= new JLabel("Autor");
		private JLabel				lblTitle			= new JLabel("Titulo");
		
		private JLabel				lblEditorial		= new JLabel(
																"Editorial");
		private Button				button				= new Button("Buscar");
		private JLabel				lblImage			= new JLabel("");
		
		private JLabel				lblUse				= new JLabel("Usuario");
		private JLabel				lblNameUser			= new JLabel(
																"-- nombre de usuario --");
		
		private JMenuBar			menuBar				= new JMenuBar();
		
		private JMenu				menu;
		
		public Principal()
			{
				initialize();
			}
		
		private void initialize()
			{
				
				this.setBounds(100, 100, 450, 300);
				this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				this.setTitle("Sistema de Gestion de Bibliotecas (SGB).");
				this.setExtendedState(JFrame.MAXIMIZED_BOTH);
				this.setLocationRelativeTo(null);
				this.setJMenuBar(menuBar);
				this.button.addActionListener(action());
				this.updateImage("/images/usuario.png");
				this.initLayout();
				
			}
		public ActionListener action()
			{
				return new ActionListener()
					{

						@Override
						public void actionPerformed(ActionEvent e)
							{
								
								
							}
					}
			}
		public void updateImage(String url)
			{
				lblImage.setIcon(new ImageIcon(Principal.class.getResource(url)));
			}
		
		public void setIcon(String url)
			{
				setIconImage(Toolkit.getDefaultToolkit().getImage(
						Principal.class.getResource(url)));
			}
		
		public void addTab(String title, Component component)
			{
				tabContainerFrom.add(title, component);
				tabContainerFrom.setTabComponentAt( tabContainerFrom.getTabCount() - 1,	new ButtonTabComponent(tabContainerFrom));
			}
		
		public void display()
			{
				setVisible(true);
				
			}
		
		public Principal addMenu(String name)
			{
				menu = new JMenu(name);
				return this;
			}
		
		public Principal addMenuItem(String name, ActionListener actionListener)
			{
				JMenuItem itemMenu = new JMenuItem(name);
				itemMenu.addActionListener(actionListener);
				menu.add(itemMenu);
				return this;
			}
		
		public void build()
			{
				menuBar.add(menu);
			}
		
		public void initLayout()
			{
				GroupLayout groupLayout = new GroupLayout(getContentPane());
				
				groupLayout
						.setHorizontalGroup(groupLayout
								.createParallelGroup(Alignment.LEADING)
								.addGroup(
										groupLayout
												.createSequentialGroup()
												.addGap(12)
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.LEADING)
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(17)
																				.addComponent(
																						lblImage,
																						GroupLayout.PREFERRED_SIZE,
																						197,
																						GroupLayout.PREFERRED_SIZE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(98)
																				.addComponent(
																						lblUse,
																						GroupLayout.PREFERRED_SIZE,
																						56,
																						GroupLayout.PREFERRED_SIZE))
																.addComponent(
																		lblNameUser,
																		GroupLayout.PREFERRED_SIZE,
																		235,
																		GroupLayout.PREFERRED_SIZE)
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(21)
																				.addComponent(
																						lblBuscar,
																						GroupLayout.PREFERRED_SIZE,
																						166,
																						GroupLayout.PREFERRED_SIZE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addComponent(
																						lblAuthor,
																						GroupLayout.PREFERRED_SIZE,
																						40,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(12)
																				.addComponent(
																						jTextAuthor,
																						GroupLayout.DEFAULT_SIZE,
																						183,
																						Short.MAX_VALUE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addComponent(
																						lblTitle,
																						GroupLayout.PREFERRED_SIZE,
																						40,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(12)
																				.addComponent(
																						jTextTitle,
																						GroupLayout.DEFAULT_SIZE,
																						183,
																						Short.MAX_VALUE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addComponent(
																						lblEditorial,
																						GroupLayout.PREFERRED_SIZE,
																						40,
																						GroupLayout.PREFERRED_SIZE)
																				.addGap(12)
																				.addComponent(
																						jTextEditorial,
																						GroupLayout.DEFAULT_SIZE,
																						183,
																						Short.MAX_VALUE))
																.addGroup(
																		groupLayout
																				.createSequentialGroup()
																				.addGap(65)
																				.addComponent(
																						button,
																						GroupLayout.PREFERRED_SIZE,
																						122,
																						GroupLayout.PREFERRED_SIZE)))
												.addGap(12)
												.addComponent(
														tabContainerFrom,
														GroupLayout.DEFAULT_SIZE,
														1619, Short.MAX_VALUE)
												.addGap(36)));
				groupLayout
						.setVerticalGroup(groupLayout
								.createParallelGroup(Alignment.LEADING)
								.addGroup(
										groupLayout
												.createSequentialGroup()
												.addGap(54)
												.addComponent(
														lblImage,
														GroupLayout.PREFERRED_SIZE,
														213,
														GroupLayout.PREFERRED_SIZE)
												.addGap(13)
												.addComponent(lblUse)
												.addGap(13)
												.addComponent(lblNameUser)
												.addGap(51)
												.addComponent(lblBuscar)
												.addGap(13)
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.LEADING)
																.addComponent(
																		lblAuthor,
																		GroupLayout.PREFERRED_SIZE,
																		22,
																		GroupLayout.PREFERRED_SIZE)
																.addComponent(
																		jTextAuthor,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE))
												.addGap(13)
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.LEADING)
																.addComponent(
																		lblTitle,
																		GroupLayout.PREFERRED_SIZE,
																		22,
																		GroupLayout.PREFERRED_SIZE)
																.addComponent(
																		jTextTitle,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE))
												.addGap(13)
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.LEADING)
																.addComponent(
																		lblEditorial,
																		GroupLayout.PREFERRED_SIZE,
																		22,
																		GroupLayout.PREFERRED_SIZE)
																.addComponent(
																		jTextEditorial,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE))
												.addGap(33)
												.addComponent(
														button,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
								.addGroup(
										groupLayout
												.createSequentialGroup()
												.addGap(26)
												.addComponent(
														tabContainerFrom,
														GroupLayout.DEFAULT_SIZE,
														928, Short.MAX_VALUE)
												.addGap(33)));
				getContentPane().setLayout(groupLayout);
				
			}
		
	}
