package pe.com.uap.form.element;

import java.io.Serializable;
import java.util.Date;

import javax.swing.JButton;

import pe.com.uap.sgb.model.Loan;

public class LoanModel implements Serializable
	{
		
		private static final long	serialVersionUID	= 1L;
		
		private String				idLoan;
		private String				title;
		private int					numberBox;
		private Date				depeartureDate;
		private Date				returnDate;
		private Loan				loan;

		
		public LoanModel(Loan loan)
			{
				this.idLoan = loan.getIdLoan();
				this.title = loan.getMaterial().getTitle();
				this.numberBox = loan.getPerson().getNumberBox();
				this.depeartureDate = loan.getDepartureDate();
				this.returnDate = loan.getReturnDate();
				this.loan = loan;

			}
		
		public String getIdLoan()
			{
				return idLoan;
			}
		
		public void setIdLoan(String idLoan)
			{
				this.idLoan = idLoan;
			}
		
		public String getTitle()
			{
				return title;
			}
		
		public void setTitle(String title)
			{
				this.title = title;
			}
		
		public int getNumberBox()
			{
				return numberBox;
			}
		
		public void setNumberBox(int numberBox)
			{
				this.numberBox = numberBox;
			}
		
		public Date getDepeartureDate()
			{
				return depeartureDate;
			}
		
		public void setDepeartureDate(Date depeartureDate)
			{
				this.depeartureDate = depeartureDate;
			}
		
		public Date getReturnDate()
			{
				return returnDate;
			}
		
		public void setReturnDate(Date returnDate)
			{
				this.returnDate = returnDate;
			}
		
		public Loan getLoan()
			{
				return loan;
			}
		
		public void setLoan(Loan loan)
			{
				this.loan = loan;
			}
		
//		public JButton getDeliver()
//			{
//				return deliver;
//			}
//		
//		public void setDeliver(JButton deliver)
//			{
//				this.deliver = deliver;
//			}
		
	}
