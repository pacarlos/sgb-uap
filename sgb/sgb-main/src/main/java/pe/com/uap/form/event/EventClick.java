package pe.com.uap.form.event;

import java.awt.event.ActionEvent;

import javax.ejb.Stateless;
import javax.inject.Inject;

import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.service.MaterialService;

@Stateless
public class EventClick
	{
		
		@Inject
		private MaterialService	service;
		
		public void setActionEvent(ActionEvent e)
			{
				service.find(new Material("M-1"));
				
			}
		
	}
