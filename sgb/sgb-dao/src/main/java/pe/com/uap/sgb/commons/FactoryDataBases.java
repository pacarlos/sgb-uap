package pe.com.uap.sgb.commons;


import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import pe.com.commoms.control.data.abstracts.DBAccess;


@Singleton
public class FactoryDataBases<E> extends DBAccess<E>
	{
		private final EntityManagerFactory	emf	= Persistence
														.createEntityManagerFactory("sbg-model");
		
		public EntityManager entityManager()
			{
				
				return emf.createEntityManager();
			}

		
	}
