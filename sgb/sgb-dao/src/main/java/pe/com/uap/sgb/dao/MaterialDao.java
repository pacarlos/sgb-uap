package pe.com.uap.sgb.dao;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.TypedQuery;

import pe.com.commoms.control.data.Data;
import pe.com.uap.sgb.commons.FactoryDataBases;
import pe.com.uap.sgb.model.Material;

public interface MaterialDao extends Data<Material> {
	
	@Override
	public Material save(Material entity);

	@Override
	public Material delete(Material entity);

	@Override
	public Material update(Material entity);

	@Override
	public Material find(Material entity);

	public List<Material> findAll();

	public List<Material> findMaterial(Material material);

	
}

@Singleton
class MaterialDaoImpl extends FactoryDataBases<Material> implements MaterialDao {

	@Override
	public List<Material> findAll()
		{
			TypedQuery<Material>  type = entityManager().createNamedQuery("Material.findAll", Material.class);
			return type.getResultList();
		}

		@Override
		public List<Material> findMaterial(Material material)
			{
				TypedQuery<Material> type = entityManager().createNamedQuery(
						"Material.findMaterial", Material.class);
				
				if (material.getAutor() != null)
					type.setParameter("autor", material.getAutor());
				
				if (material.getTitle() != null)
					type.setParameter("title", material.getTitle());
				
				if (material.getBox() != null
						&& material.getBox().getEditorial() != null)
					type.setParameter("editorial", material.getBox()
							.getEditorial());
				
				return type.getResultList();
			}

	

	

}