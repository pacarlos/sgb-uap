package pe.com.uap.sgb.dao;

import java.util.List;

import javax.inject.Singleton;
import javax.persistence.TypedQuery;

import pe.com.commoms.control.data.Data;
import pe.com.uap.sgb.commons.FactoryDataBases;
import pe.com.uap.sgb.model.Person;

public interface PersonDao extends Data<Person>
	{
		@Override
		public Person save(Person entity);
		
		@Override
		public Person update(Person entity);
		
		@Override
		public Person delete(Person entity);
		
		@Override
		public Person find(Person entity);
		
		public List<Person> findAll();
	}

@Singleton
class PersonDaoImpl extends FactoryDataBases<Person> implements PersonDao
	{
		
		@Override
		public List<Person> findAll()
			{
				TypedQuery<Person>  type = entityManager().createNamedQuery("Person.findAll",Person.class);
				return type.getResultList();
			}
		
	}
