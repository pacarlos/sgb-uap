package pe.com.uap.sgb.dao;

//import javax.ejb.Singleton;

import java.util.List;

import javax.inject.Singleton;
import javax.persistence.TypedQuery;

import pe.com.commoms.control.data.Data;
import pe.com.commoms.control.data.abstracts.DBAccess;
import pe.com.uap.sgb.commons.FactoryDataBases;
import pe.com.uap.sgb.model.Loan;
import pe.com.uap.sgb.model.Material;
//import pe.com.uap.sgb.model.Material;
import pe.com.uap.sgb.model.Person;

public interface LoanDao extends Data<Loan> {
	@Override
	public Loan save(Loan entity);

	@Override
	public Loan update(Loan entity);

	@Override
	public Loan delete(Loan entity);

	@Override
	public Loan find(Loan entity);

	public List<Loan> findAll();

	public List<Loan> findAllByPerson(Person person);
}
@Singleton
class LoanDaoImpl extends FactoryDataBases<Loan> implements LoanDao {

	@Override
	public List<Loan> findAll()
		{
			TypedQuery<Loan>  type = entityManager().createNamedQuery("Loan.findAll", Loan.class);
			return type.getResultList();
		}

	@Override
	public List<Loan> findAllByPerson(Person person)
		{
			System.out.println(person.getDni());
			TypedQuery<Loan>  type = entityManager().createNamedQuery("Loan.findAllByPerson", Loan.class);
			type.setParameter("dni", person.getDni());
			return type.getResultList();
		}

}
