package pe.com.uap.sgb.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the student database table.
 * 
 */
@Entity
@NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
public class Student implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "dni"
//	, updatable = false, insertable = false
	)
	private int dni;

	private int registration;

	// bi-directional one-to-one association to Person
	@OneToOne
	@JoinColumn(name = "dni", updatable = false, insertable = false)
	private Person person;

	public Student() {
	}

	public int getDni() {
		return this.dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getRegistration() {
		return this.registration;
	}

	public void setRegistration(int registration) {
		this.registration = registration;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}