package pe.com.uap.sgb.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the box database table.
 * 
 */
@Entity
@NamedQuery(name = "Box.findAll", query = "SELECT b FROM Box b")
public class Box implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_material", updatable = true, insertable = true)
	private String idMaterial;

	private String editorial;

	// bi-directional one-to-one association to Material
	@OneToOne
	@JoinColumn(name = "id_material", insertable = false, updatable = false)
	private Material material;

	public Box() {
	}

	public String getIdMaterial() {
		return this.idMaterial;
	}

	public void setIdMaterial(String idMaterial) {
		this.idMaterial = idMaterial;
	}

	public String getEditorial() {
		return this.editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public Material getMaterial() {
		return this.material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

}