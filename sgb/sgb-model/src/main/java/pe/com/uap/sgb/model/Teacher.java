package pe.com.uap.sgb.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the teacher database table.
 * 
 */
@Entity
@NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t")
public class Teacher implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "dni"
//	, updatable = false, insertable = false
	)
	private int dni;

	@Column(name = "number_employees")
	private int numberEmployees;

	// bi-directional one-to-one association to Person
	@OneToOne
	@JoinColumn(name = "dni", updatable = false, insertable = false)
	private Person person;

	public Teacher() {
	}

	public int getDni() {
		return this.dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getNumberEmployees() {
		return this.numberEmployees;
	}

	public void setNumberEmployees(int numberEmployees) {
		this.numberEmployees = numberEmployees;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}