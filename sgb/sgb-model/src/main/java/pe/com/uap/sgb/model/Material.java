package pe.com.uap.sgb.model;

import java.io.Serializable;

import javax.persistence.*;

import pe.com.uap.sgb.enums.TypeMaterial;
import pe.com.uap.sgb.enums.TypeStatus;

import java.util.List;

/**
 * The persistent class for the material database table.
 * 
 */
@Entity
@NamedQueries(
	{
			@NamedQuery(name = "Material.findAll", query = "SELECT m FROM Material m"),
			@NamedQuery(name = "Material.findMaterial", query = "SELECT m FROM Material m WHERE m.autor=:autor OR m.title=:title OR m.box.editorial=:editorial") })
public class Material implements Serializable , Cloneable
	{
		private static final long	serialVersionUID	= 1L;
		
		@Id
		@Column(name = "id_material", updatable = true, insertable = true)
		private String				idMaterial;
		
		private int					age;
		
		private String				autor;
		@Enumerated(EnumType.STRING)
		private TypeStatus			status;
		
		private String				title;
		
		@Column(name = "type_material")
		@Enumerated(EnumType.STRING)
		private TypeMaterial		typeMaterial;
		
		// bi-directional one-to-one association to Box
		@OneToOne(mappedBy = "material", cascade = CascadeType.PERSIST)
		private Box					box;
		
		// bi-directional many-to-one association to Loan
		@OneToMany(mappedBy = "material")
		private List<Loan>			loans;
		
		// bi-directional one-to-one association to Magazine
		@OneToOne(mappedBy = "material", cascade = CascadeType.PERSIST)
		private Magazine			magazine;
		
		public Material()
			{
			}
		
		public Material(String idMaterial)
			{
				setIdMaterial(idMaterial);
			}
		
		public String getIdMaterial()
			{
				return this.idMaterial;
			}
		
		public void setIdMaterial(String idMaterial)
			{
				this.idMaterial = idMaterial;
			}
		
		public int getAge()
			{
				return this.age;
			}
		
		public void setAge(int age)
			{
				this.age = age;
			}
		
		public String getAutor()
			{
				return this.autor;
			}
		
		public void setAutor(String autor)
			{
				this.autor = autor;
			}
		
		public TypeStatus getStatus()
			{
				return status;
			}
		
		public void setStatus(TypeStatus status)
			{
				this.status = status;
			}
		
		public TypeMaterial getTypeMaterial()
			{
				return typeMaterial;
			}
		
		public void setTypeMaterial(TypeMaterial typeMaterial)
			{
				this.typeMaterial = typeMaterial;
			}
		
		public String getTitle()
			{
				return this.title;
			}
		
		public void setTitle(String title)
			{
				this.title = title;
			}
		
		public Box getBox()
			{
				return this.box;
			}
		
		public void setBox(Box box)
			{
				this.box = box;
				this.box.setIdMaterial(getIdMaterial());
			}
		
		public List<Loan> getLoans()
			{
				return this.loans;
			}
		
		public void setLoans(List<Loan> loans)
			{
				this.loans = loans;
			}
		
		public Loan addLoan(Loan loan)
			{
				getLoans().add(loan);
				loan.setMaterial(this);
				
				return loan;
			}
		
		public Loan removeLoan(Loan loan)
			{
				getLoans().remove(loan);
				loan.setMaterial(null);
				
				return loan;
			}
		
		public Magazine getMagazine()
			{
				return this.magazine;
			}
		
		public void setMagazine(Magazine magazine)
			{
				this.magazine = magazine;
				this.magazine.setIdMaterial(getIdMaterial());
			}
		
		public void replace()
			{
				setStatus(TypeStatus.REMPLACE);
				
			}
		
		public void subscribe()
			{
				setStatus(TypeStatus.SUBCRIBE);
				
			}
		
		public void unSubcribe()
			{
				setStatus(TypeStatus.UNSUBCRIBE);
				
			}
		
	}