package pe.com.uap.sgb.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the loan database table.
 * 
 */
@Entity
@NamedQueries(
	{
			@NamedQuery(name = "Loan.findAll", query = "SELECT l FROM Loan l"),
			@NamedQuery(name = "Loan.findAllByPerson", query = "SELECT l FROM Loan l WHERE l.person.dni=:dni")
	})
public class Loan implements Serializable , Cloneable
	{
		private static final long	serialVersionUID	= 1L;
		
		@Id
		@Column(name = "id_loan")
		private String				idLoan;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "departure_date")
		private Date				departureDate;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "return_date")
		private Date				returnDate;
		
		// bi-directional many-to-one association to Material
		@ManyToOne
		@JoinColumn(name = "id_material", insertable = false, updatable = false)
		private Material			material;
		
		// bi-directional many-to-one association to Person
		@ManyToOne
		@JoinColumn(name = "dni", insertable = false, updatable = false)
		private Person				person;
		
		public Loan()
			{
			}
		
		public String getIdLoan()
			{
				return this.idLoan;
			}
		
		public void setIdLoan(String idLoan)
			{
				this.idLoan = idLoan;
			}
		
		public Date getDepartureDate()
			{
				return this.departureDate;
			}
		
		public void setDepartureDate(Date departureDate)
			{
				this.departureDate = departureDate;
			}
		
		public Date getReturnDate()
			{
				return this.returnDate;
			}
		
		public void setReturnDate(Date returnDate)
			{
				this.returnDate = returnDate;
			}
		
		public Material getMaterial()
			{
				return this.material;
			}
		
		public void setMaterial(Material material)
			{
				this.material = material;
			}
		
		public Person getPerson()
			{
				return this.person;
			}
		
		public void setPerson(Person person)
			{
				this.person = person;
			}
		
	}