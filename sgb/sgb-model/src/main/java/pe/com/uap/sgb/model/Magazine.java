package pe.com.uap.sgb.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the magazine database table.
 * 
 */
@Entity
@NamedQuery(name = "Magazine.findAll", query = "SELECT m FROM Magazine m")
public class Magazine implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_material", updatable =true, insertable = true)
	private String idMaterial;

	// bi-directional one-to-one association to Material
	@OneToOne
	@JoinColumn(name = "id_material", insertable = false, updatable = false)
	private Material material;

	public Magazine() {
	}

	public String getIdMaterial() {
		return this.idMaterial;
	}

	public void setIdMaterial(String idMaterial) {
		this.idMaterial = idMaterial;
	}

	public Material getMaterial() {
		return this.material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

}