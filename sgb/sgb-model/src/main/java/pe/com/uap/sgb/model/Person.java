package pe.com.uap.sgb.model;

import java.io.Serializable;

import javax.persistence.*;

import pe.com.uap.sgb.enums.TypePerson;

import java.util.List;

/**
 * The persistent class for the person database table.
 * 
 */
@Entity
@NamedQuery(name = "Person.findAll", query = "SELECT p FROM Person p")
public class Person implements Serializable , Cloneable
	{
		private static final long	serialVersionUID	= 1L;
		
		@Id
		@Column(name = "dni"
		// , updatable = false, insertable = false
		)
		private int					dni;
		
		private String				email;
		
		@Column(name = "last_name")
		private String				lastName;
		
		private String				name;
		
		@Column(name = "number_box")
		private int					numberBox;
		
		private int					phone;
		
		@Column(name = "type_person")
		@Enumerated(EnumType.STRING)
		private TypePerson			typePerson;
		
		// bi-directional many-to-one association to Loan
		@OneToMany(mappedBy = "person")
		private List<Loan>			loans;
		
		// bi-directional one-to-one association to Student
		@OneToOne(mappedBy = "person", cascade = CascadeType.PERSIST)
		private Student				student;
		
		// bi-directional one-to-one association to Teacher
		@OneToOne(mappedBy = "person", cascade = CascadeType.PERSIST)
		private Teacher				teacher;
		
		public Person()
			{
			}
		
		public Person(int dni)
			{
				this.dni = dni;
			}

		public int getDni()
			{
				return this.dni;
			}
		
		public void setDni(int dni)
			{
				this.dni = dni;
			}
		
		public String getEmail()
			{
				return this.email;
			}
		
		public void setEmail(String email)
			{
				this.email = email;
			}
		
		public String getLastName()
			{
				return this.lastName;
			}
		
		public void setLastName(String lastName)
			{
				this.lastName = lastName;
			}
		
		public String getName()
			{
				return this.name;
			}
		
		public void setName(String name)
			{
				this.name = name;
			}
		
		public int getNumberBox()
			{
				return this.numberBox;
			}
		
		public void setNumberBox(int numberBox)
			{
				this.numberBox = numberBox;
			}
		
		public int getPhone()
			{
				return this.phone;
			}
		
		public void setPhone(int phone)
			{
				this.phone = phone;
			}
		
		public TypePerson getTypePerson()
			{
				return typePerson;
			}
		
		public void setTypePerson(TypePerson typePerson)
			{
				this.typePerson = typePerson;
			}
		
		public List<Loan> getLoans()
			{
				return this.loans;
			}
		
		public void setLoans(List<Loan> loans)
			{
				this.loans = loans;
			}
		
		public Loan addLoan(Loan loan)
			{
				getLoans().add(loan);
				loan.setPerson(this);
				
				return loan;
			}
		
		public Loan removeLoan(Loan loan)
			{
				getLoans().remove(loan);
				loan.setPerson(null);
				
				return loan;
			}
		
		public Student getStudent()
			{
				return this.student;
			}
		
		public void setStudent(Student student)
			{
				this.student = student;
			}
		
		public Teacher getTeacher()
			{
				return this.teacher;
			}
		
		public void setTeacher(Teacher teacher)
			{
				this.teacher = teacher;
			}
		
		public void deliver()
			{
				int number = getNumberBox();
				setNumberBox(number--);
			}
		
		public void carry()
			{
				int number = getNumberBox();
				setNumberBox(number++);
			}
	}