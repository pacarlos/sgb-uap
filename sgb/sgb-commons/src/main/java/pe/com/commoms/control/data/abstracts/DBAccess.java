package pe.com.commoms.control.data.abstracts;

import pe.com.commoms.control.data.Data;


public abstract class DBAccess<E> extends Persist<E> implements Data<E> {}

