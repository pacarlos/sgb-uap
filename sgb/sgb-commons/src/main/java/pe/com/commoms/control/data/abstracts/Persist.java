package pe.com.commoms.control.data.abstracts;

//import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

//import javax.ejb.TransactionAttribute;
//import javax.ejb.TransactionAttributeType;

/**
 * @author cpena
 *
 */
abstract class Persist<E> extends Finder<E>
	{
		
		public String id()
			{
				return getEntity().getSimpleName() + "-" + count();
			}
		
		public E delete(E entity)
			{
				EntityManager entityManager = entityManager();
				EntityTransaction entityTransaction = entityManager
						.getTransaction();
				try
					{
						entityTransaction.begin();
						entityManager.remove(entity);
					} catch (Exception e)
					{
						e.printStackTrace();
						entityTransaction.rollback();
					} finally
					{
						entityTransaction.commit();
					}
				
				return entity;
			}
		
		public E save(E entity)
			{
				EntityManager entityManager = entityManager();
				EntityTransaction entityTransaction = entityManager
						.getTransaction();
				try
					{
						entityTransaction.begin();
						entityManager.persist(entity);
					} catch (Exception e)
					{
						e.printStackTrace();
						entityTransaction.rollback();
					} finally
					{
						entityTransaction.commit();
					}
				
				return entity;
			}
		
		public E update(E entity)
			{
				EntityManager entityManager = entityManager();
				EntityTransaction entityTransaction = entityManager
						.getTransaction();
				try
					{
						entityTransaction.begin();
						entityManager.merge(entity);
					} catch (Exception e)
					{
						e.printStackTrace();
						entityTransaction.rollback();
					} finally
					{
						entityTransaction.commit();
					}
				
				return entity;
			}
	}
