package pe.com.commoms.control.data.abstracts;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;



import javax.persistence.Query;

import pe.com.commoms.control.data.interceptor.InterceptorPrimaryKey;

abstract class Finder<E> extends EntityDataAccess
	{
		private Type[]	typeArguments;
			
			{
				Type typeClass = this.getClass().getGenericSuperclass();
				
				typeArguments = ((ParameterizedType) typeClass).getActualTypeArguments();
				
				InterceptorPrimaryKey.register(getEntity());
			}
		
		public Long count()
			{
				Query query = entityManager().createQuery(sqlFormat());
				return (Long) query.getSingleResult();
				
			}
		
		private String sqlFormat()
			{
				return "select count(e) from " + getEntity().getSimpleName() + " e";
			}
		
		public E find(E e)
			{
				Object pk = getPrimaryKeyInstance(e);
				System.out.println(pk);
				System.out.println(entityManager());
				System.out.println(getEntity());
				return entityManager().find(getEntity(), pk);
			}
		
		@SuppressWarnings("unchecked")
		protected Class<E> getEntity()
			{
				return (Class<E>) typeArguments[0];
			}
		
		private Object getPrimaryKeyInstance(E e)
			{
				try
					{
						Field pk = InterceptorPrimaryKey.getPK(getEntity());
						
						pk.setAccessible(true);
						
						return pk.get(e);
					} catch (IllegalArgumentException | IllegalAccessException ex)
					{
						ex.printStackTrace();
					}
				
				return null;
			}
	}
