package pe.com.commoms.control.data.interceptor;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFormat {

	private String format;

	private static final String fieldStart = "\\$\\{";
	private static final String fieldEnd = "\\}";

	private static final String regex = fieldStart + "([^}]+)" + fieldEnd;
	private static final Pattern pattern = Pattern.compile(regex);

	private StringFormat(String template) {
		this.format = template;
	}

	public static StringFormat template(String template) {
		return new StringFormat(template);
	}

	public String format(Map<String, Object> objects) {
		Matcher m = pattern.matcher(format);
		String result = format;
		while (m.find()) {
			try {
				String[] found = m.group(1).split("\\.");
				
				Object o = objects.get(found[0]);
				Field f = o.getClass().getField(found[1]);
				String newVal = f.get(o).toString();
				result = result.replaceFirst(regex, newVal);
			} catch (NoSuchFieldException | SecurityException
					| IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}