package pe.com.commoms.control.data.abstracts;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

abstract class EntityDataAccess {

	private final static Properties PROPERTIES = readProperties();
	
	
	@PersistenceContext
	private EntityManager entityManager;

	@Produces
	@RequestScoped
	public EntityManager entityManager() {
		return entityManager;
	}

	private static final Properties readProperties() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = EntityDataAccess.class
					.getResourceAsStream("/template-table.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return prop;

	}

	protected Properties getProperties() {
		return PROPERTIES;
	}

}
