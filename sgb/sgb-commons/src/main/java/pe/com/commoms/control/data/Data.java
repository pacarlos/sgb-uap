package pe.com.commoms.control.data;

public interface Data<E>
	{
		public abstract E delete(E entity);
		
		public abstract E find(E entity);
		
		public abstract E save(E entity);
		
		public abstract E update(E entity);
		
		public abstract Long count();
		
		public String id();
	}