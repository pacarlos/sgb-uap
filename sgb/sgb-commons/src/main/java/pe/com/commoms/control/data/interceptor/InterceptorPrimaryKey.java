package pe.com.commoms.control.data.interceptor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

public class InterceptorPrimaryKey {
    private final static InterceptorPrimaryKey INSTANCE = new InterceptorPrimaryKey();
    private final Map<String, Field>           hashMap  = new HashMap<String, Field>();

    public static void register(Class<?> cls) {
        try {
            if (!INSTANCE.validatePkByEntity(cls)) {
                throw new Exception("not fount primary key (@Id ,@EmbeddedId) in @Entity");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerPK(Class<?> cls, Field field) {
        INSTANCE.hashMap.put(cls.getName(), field);
    }

    public boolean validatePkByEntity(Class<?> cls) {
        for (Field field : cls.getDeclaredFields()) {
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if ((annotation instanceof Id) || (annotation instanceof EmbeddedId)) {
                    registerPK(cls, field);

                    return true;
                }
            }
        }

        return false;
    }

    public static Field getPK(Class<?> cls) {
        return INSTANCE.hashMap.get(cls.getName());
    }
}

